<?php 

namespace App\PW\Helpers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Helpers {

	public $number = null;
	public $unit = null;
	public $abbreviation = null;


	private function getUserUnit()
	{
		$user = User::find( auth()->id() );

		$unit = ( $user->preferences->unit ) ? $user->preferences->unit : 'inches';
	}

	private function setUserUnit( $unit = null )
	{

		switch( $request->unit )
		{
			case 'centimeters':
				$unit = $request->unit;
				break;
			case 'inches':
			default:
				$unit = 'inches';
				break;
		}
	}

	private function convertInchesToUnit( $number, $unit = null )
	{
		switch( $unit ) {
			case "centimeters":
				$number = round( $number / 2.54, 2 );
				$abbreviation = 'cm';
				break;
			case "meters":
				$number = round( $number / 2.54 * 100, 2 );
				$abbreviation = 'm';
				break;
			case "yards":
				$number = round( $number * 0.027778, 2 );
				$abbreviation = 'yd';
				break;
		}

		return array( 'number' => $number, 'unit' => $unit, 'abbreviation' => $abbreviation );

	}

	private function convertUnitToInches( $number, $unit = null )
	{
		if ( is_null( $unit ) || empty( $unit ) )
		{
			$user = User::find( auth()->id() );

			$unit = ( $user->preferences->unit ) ? $user->preferences->unit : 'inches';
		}

		switch( $unit ) {
			case "centimeters":
				$number = round( $number * 2.54, 2 );
				break;
			case "meters":
				$number = round( $number / 2.54 * 100, 2 );
				break;
			case "yards":
				$number = round( $number * 0.027778, 2 );
				break;
		}

		return array( 'number' => $number, 'unit' => $unit, 'abbreviation' => $abbreviation );
	}

	public function convertUnitToUnit( $number, $unit = null )
	{
		if ( is_null( $unit ) || empty( $unit ) )
		{
			$user = User::find( auth()->id() );

			$unit = ( $user->preferences->unit ) ? $user->preferences->unit : 'inches';
		}

		switch( $unit ) {
			case "centimeters":
				$number = round( $number * 2.54, 2 );
				break;
			case "meters":
				$number = round( $number / 2.54 * 100, 2 );
				break;
			case "yards":
				$number = round( $number * 0.027778, 2 );
				break;
		}

		return array( 'number' => $number, 'unit' => $unit, 'abbreviation' => $abbreviation );
	}
}