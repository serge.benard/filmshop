<?php

namespace App\PW\Helpers;

use Illuminate\Support\ServiceProvider;

class UnitsServiceProvider extends ServiceProvider {

	public function register()
	{
		$this->app->bind( 'Units', Units::class );
	}

}