<?php

namespace App\PW\Helpers;

use Illuminate\Support\ServiceProvider;

class HelpersServiceProvider extends ServiceProvider {

	public function register()
	{
		$this->app->bind( 'Helpers', Helpers::class );
	}

}