<?php 

namespace App\PW\Helpers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Units {

	static $number = null;
	static $unit = null;
	static $abbreviation = null;


	public function __construct()
	{
		self::$unit = $this->getUserUnit();

		self::$abbreviation = $this->setAbbreviation( self::$unit );
	}

	public function getUserUnit()
	{
		return Auth::user()->preferences->unit;
	}

	public function unitList()
	{
		return array(
			'inches',
			'feet',
			'yards',
			'centimeters',
			'meters'
		);
	}

	public function getAbbreviation()
	{
		// dump( '$unit: ' . self::$unit, '$abbreviation: ' . self::$abbreviation );
		return self::$abbreviation;
	}

	public function setAbbreviation( $unit )
	{

		return $this->abbreviations( $unit );
	}

	public function abbreviations( $unit )
	{
		// dump( 'Unit: ' . $unit );

		$ret = null;

		switch( $unit )
		{
			case 'centimeters':
				$ret = 'cm';
				break;
			case 'meters':
				$ret = 'm';
				break;
			case 'yards':
				$ret = 'yd';
				break;
			case 'inches':
				$ret = '"';
				break;
			case 'feet':
				$ret = "'";
				break;
		}

		// dump( '$ret: ' . $ret );

		return $ret;
	}

	public static function convertInchesToUnit( $number, $unit = null )
	{
		if ( is_null( $unit ) )
		{
			$unit = self::$unit;
		}

		switch( $unit ) {
			case 'centimeters':
				$number = round( $number * 2.54, 3 );
				break;
			case 'meters':
				$number = round( $number * 0.0254, 3 );
				break;
			case 'feet':
				$number = round( $number / 12 , 3);
				break;
			case 'yards':
				$number = round( $number / 36, 3 );
				break;
		}

		return $number;

	}

	public function convertUnitToInches( $number, $unit = null )
	{

		switch( $unit ) {
			case 'centimeters':
				$number = round( $number / 2.54, 3 );
				break;
			case 'meters':
				$number = round( $number / 0.0254, 3 );
				break;
			case 'feet':
				$number = round( $number * 12, 3 );
				break;
			case 'yards':
				$number = round( $number * 36, 3 );
				break;
		}

		return $number;
	}

	public function convert( $number, $type = 'get', $unit = null )
	{
		switch ( $type )
		{
			case 'set':
				$number = convertUnitToInches( $number, $unit );
				break;
			case 'get':
				$number = convertInchesToUnit( $number, $this->unit );
				break;
		}

		return $number;
	}
}