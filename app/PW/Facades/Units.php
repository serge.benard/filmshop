<?php

namespace App\PW\Facades;

use Illuminate\Support\Facades\Facade;


class Units extends Facade {

	protected static function getFacadeAccessor()
	{
		return 'Units';
	}

}