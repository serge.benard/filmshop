<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preferences extends Model
{
    //
    protected $fillable = [
		'user_id',
		'unit',
	];

	public $timestamps = false;

	public function user()
	{
		return $this->belongsTo( User::class );
	}
}
