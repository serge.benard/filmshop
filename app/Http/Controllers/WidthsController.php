<?php

namespace App\Http\Controllers;

use App\PW\Facades\Units;
use App\User;
use App\Widths;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WidthsController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
		$widths = Widths::all();

		return view('widths.index', compact('widths'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
		$preferences = Auth::user()->preferences;

		return view( 'widths.new', compact('preferences'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
		$this->validate($request, [
			'amount' => 'required|between:0.001,20000',
		]);

		$width = new Widths;

		/*$amount = Helpers::convertUnit( $request->amount );*/

		$width->amount = Units::convertUnitToInches( $request->amount, $request->units );

		$width->save();

		return redirect( route('widths.index') )->with('message', 'Successfully added a new Film Width.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Widths  $widths
	 * @return \Illuminate\Http\Response
	 */
	public function show(Widths $widths)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Widths  $widths
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Widths $width)
	{
		//

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Widths  $widths
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Widths $widths)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Widths  $widths
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Widths $width)
	{
		//
		$message = 'Could not delete Film Width. Width ID: ' . $width->id;

		if ( $width->delete() )
		{
			$message = 'Successfully deleted Film Width.';
		}

		return redirect( route( 'widths.index' ) )->with( 'message', $message );
	}
}
