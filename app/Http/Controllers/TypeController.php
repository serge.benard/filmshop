<?php

namespace App\Http\Controllers;

use App\Category;
use App\Type;
use Illuminate\Http\Request;

class TypeController extends Controller
{

	/*
	* Style for Types
	* Identifier: fa-tag
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	    $this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$types = Type::paginate(20);

		//
		return view( 'types.index', compact('types') );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//

		$categories = Category::orderBy('name', 'ASC')->get();

		return view( 'types.new', compact('categories') );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
		$this->validate($request, [
			'category' => 'required|exists:categories,id',
			'name' => 'required|min:2|unique:types,name|max:200',
			'abbreviation' => 'required|unique:types,abbreviation|min:1|max:7|alpha_num',
			'description' => 'nullable|min:2|max:200',
		]);

		$type = new Type;

		$type->category_id = $request->category;
		$type->name = $request->name;
		$type->abbreviation = $request->abbreviation;
		$type->description = $request->description;

		$type->save();

		return redirect( route('types.index') )->with('message', 'Successfully created a new Film Type.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Type  $type
	 * @return \Illuminate\Http\Response
	 */
	public function show(Type $type)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Type  $type
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Type $type)
	{
		//
		$categories = Category::orderBy('name', 'ASC')->get();

		return view('types.edit', compact('type', 'categories'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Type  $type
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Type $type)
	{
		//
		$this->validate($request, [
			'category' => 'required|exists:categories,id',
			'name' => 'required|min:2|max:200|unique:types,name,' . $type->id . ',id',
			'abbreviation' => 'required|min:1|max:7|alpha_num|unique:types,abbreviation,' . $type->id . ',id',
			'description' => 'nullable|min:2|max:200',
		]);

		$type->category_id = $request->category;
		$type->name = $request->name;
		$type->abbreviation = $request->abbreviation;
		$type->description = $request->description;

		$type->save();

		return redirect( route('types.index') )->with('message', 'Successfully updated the Film Type.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Type  $type
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Type $type)
	{
		//
		$type->delete();

		return redirect( route( 'types.index' ) )->with('message', 'Successfully deleted Film Type.');
	}
}
