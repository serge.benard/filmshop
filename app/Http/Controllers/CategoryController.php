<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

	/*
	* Style for Categories
	* Identifier: fa-tag
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	    $this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$categories = Category::paginate(20);

		//
		return view( 'categories.index', compact('categories') );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
		return view( 'categories.new' );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
		$this->validate($request, [
			'name' => 'required|min:2|unique:categories,name|max:200',
			'description' => 'nullable|min:2|max:200',
		]);

		$category = new Category;

		$category->name = $request->name;
		$category->description = $request->description;

		$category->save();

		return redirect( route('categories.index') )->with('message', 'Successfully created a new Film Category.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Category  $category
	 * @return \Illuminate\Http\Response
	 */
	public function show(Category $category)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Category  $category
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Category $category)
	{
		//
		return view('categories.edit', compact('category'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Category  $category
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Category $category)
	{
		//
		$this->validate($request, [
			'name' => 'required|min:2|max:200|unique:categories,name,' . $category->id . ',id',
			'description' => 'nullable|min:2|max:200',
		]);

		$category->name = $request->name;
		$category->description = $request->description;

		$category->save();

		return redirect( route('categories.index') )->with('message', 'Successfully updated the Film Category.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Category  $category
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Category $category)
	{
		//
		$category->delete();

		return redirect( route( 'categories.index' ) )->with('message', 'Successfully deleted Film Category.');
	}
}
