<?php

namespace App\Http\Controllers;

use App\Preferences;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PreferencesController extends Controller
{
   /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
		if ( ! $preferences = Preferences::where( 'user_id', Auth::user()->id )->first() )
		{
			$preference = new Preferences;

			$preference->user_id = Auth::user()->id;
			$preference->unit = 'inches';

			$preference->save();

			$preferences = Preferences::where( 'user_id', Auth::user()->id )->first();
		}

		return view('preferences.index', compact('preferences'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	public function editUnit( Preferences $preferences )
	{
		//
		return view('preferences.editUnit', compact('preferences'));
	}

	public function updateUnit( Request $request, Preferences $preferences )
	{
		//
		$this->validate($request, [
			'unit' => 'required|in:inches,centimeters',
		]);

		$preferences->unit = $request->unit;

		$preferences->save();

		return redirect( route('preferences.index') )->with('message', 'Successfully changed preferred unit of measure.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Preferences  $preferences
	 * @return \Illuminate\Http\Response
	 */
	public function show(Preferences $preferences)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Preferences  $preferences
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Preferences $preferences)
	{
		//
		return view( 'preferences.edit', compact( 'preferences' ));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Preferences  $preferences
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Preferences $preferences)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Preferences  $preferences
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Preferences $preferences)
	{
		//
	}
}
