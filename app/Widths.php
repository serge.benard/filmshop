<?php

namespace App;

use App\PW\Helpers\Units;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Widths extends Model
{
    //
    //
	protected $fillable = [
		'amount',
	];

	public $timestamps = false;

	public function getAmountAttribute( $value )
	{
		return round( Units::convertInchesToUnit( $value ) , 3 );
	}
}
