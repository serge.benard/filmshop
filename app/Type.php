<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
	//
	protected $fillable = [
		'category_id',
		'name',
		'abbreviation',
		'description'
	];

	public $timestamps = false;
	
	public function category()
	{
		return $this->belongsTo( Category::class );
	}

	public function setNameAttribute( $value )
	{
		$this->attributes['name'] = ucwords( $value );
	}

	public function setAbbreviationAttribute( $value )
	{
		$this->attributes['abbreviation'] = preg_replace("/[^A-Z0-9]+/", "", strtoupper( $value ) );
	}
}
