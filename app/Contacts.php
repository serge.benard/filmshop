<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    //
    protected $fillable = [
		'user_id',
		'name',
		'title',
	];

	public function user()
	{
		return $this->belongsTo( User::class );
	}

	public function setNameAttribute( $value )
	{
		$this->attributes['name'] = ucwords( $value );
	}

	public function setTitleAttribute( $value )
	{
		$this->attributes['title'] = ucwords( $value );
	}
}
