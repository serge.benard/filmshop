<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Silber\Bouncer\Database\HasRolesAndAbilities;

class User extends Authenticatable
{
	use Notifiable;

	use HasRolesAndAbilities;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public function preferences()
	{
		return $this->hasOne( Preferences::class );
	}

	public function unit()
	{
		return ( Auth::check() ) ? Auth::user()->preferences->unit : 'inches';
	}

	public function privileges()
	{
		return $this->hasOne( Privileges::class );
	}
}
