<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	//
	protected $fillable = [
		'name',
		'description'
	];

	public $timestamps = false;

	public function type()
	{
		return $this->hasOne( Type::class );
	}
	
	public function setNameAttribute( $value )
	{
		$this->attributes['name'] = ucwords( $value );
	}
}
