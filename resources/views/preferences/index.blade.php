@extends( 'layouts.app' )

@section( 'pageTitle', 'Preferences' )

@section('breadcrumbs')

	<ol class="breadcrumb bg-faded">
		<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
		<li class="breadcrumb-item active">Preferences</li>
	</ol>

@endsection

@section( 'content' )
	<h1>Preferences</h1>

	<div class="card-columns">
		
		<div class="card">

			<div class="card-block">

				<h4 class="card-title h5">
					<span class="fa fa-cube"></span>
					Measuring Units
				</h4>

				<p>
					{{ ucwords( $preferences->unit ) }}
				</p>

			</div>

			<div class="card-footer">
				<div class="form-group">
					<a href="{{ route('preferences.editUnit', $preferences->id) }}" class="btn btn-secondary btn-block btn-sm">
						<span class="fa fa-pencil"></span>
						Edit
					</a>
				</div>
			</div>

		</div>
	</div>
@endsection