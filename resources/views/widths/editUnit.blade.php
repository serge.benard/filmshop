@extends( 'layouts.app' )

@section( 'pageTitle', 'Preferences' )

@section('breadcrumbs')

	<ol class="breadcrumb bg-faded">
		<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('preferences.index') }}">Preferences</a></li>
		<li class="breadcrumb-item active">Edit Units</li>
	</ol>

@endsection

@section( 'content' )
	<h1>Edit Units</h1>

	<form action="{{ route( 'preferences.updateUnit', $preferences->id ) }}" method="POST">
		{{ csrf_field() }}
		{{ method_field('PUT') }}

		@php
		$units = [
					'inches',
					'centimeters',
				];

		@endphp
		<div class="form-group{{ $errors->has('unit') ? ' has-danger' : '' }}">
			<label for="preferenceUnit">Preferred Unit of Measure</label>
			<select class="form-control" id="preferenceUnit" name="unit">
				@foreach( $units as $unit )
				<option value="{{ $unit }}" @if( $unit == $preferences->unit ) selected @endif>
					{{ ucwords( $unit ) }}
				</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-primary btn-block">
				<span class="fa fa-disk"></span>
				Update
			</button>
			<a href="{{ route('preferences.index') }}" class="btn btn-secondary btn-block">
				Cancel
			</a>
		</div>
	</form>
@endsection