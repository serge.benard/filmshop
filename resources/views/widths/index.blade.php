@extends( 'layouts.app' )

@section( 'pageTitle', 'Film Widths' )

@section('breadcrumbs')

	<ol class="breadcrumb bg-faded">
		<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
		<li class="breadcrumb-item active">Film Widths</li>
	</ol>

@endsection

@section( 'content' )
	<h1>Film Widths</h1>

	<p>
		<a href="{{ route('widths.create') }}" class="btn btn-info btn-block">
			<span class="fa fa-plus-square-o"></span>
			New Film Width
		</a>
	</p>

	<div class="card-columns">
		@foreach( $widths as $width )
		<div class="card">

			<div class="card-block">

				<h4 class="card-title h5">
					<span class="fa fa-arrows-h"></span>
					Measuring Units
				</h4>

				<p>
					{{ Units::convertInchesToUnit( $width->amount ) }} {{ Units::getAbbreviation() }}
				</p>

			</div>

			<div class="card-footer">
				<form id="deleteForm{{ $width->id }}" action="{{ route( 'widths.destroy', $width->id ) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}

					<div class="form-group">
						<a href="{{ route('widths.edit', $width->id) }}" class="btn btn-secondary btn-block btn-sm">
							<span class="fa fa-pencil"></span>
							Edit
						</a>
					</div>

					<button type="button" class="btn btn-outline-danger btn-block btn-sm" data-toggle="modal" data-target="#deleteConfirmModal" data-id="{{ $width->id }}">
						<span class="fa fa-trash"></span> 
						Delete
					</button>
					
				</form>
			</div>

		</div>
		@endforeach
	</div>

	@if ( $widths->count() < 1 )
	<p>
		There are no Film Widths to display.
	</p>
	@endif

	<p>
		<a href="{{ route('widths.create') }}" class="btn btn-info btn-block">
			<span class="fa fa-plus-square-o"></span>
			New Film Width
		</a>
	</p>
 	
	<div class="modal fade" id="deleteConfirmModal" tabindex="-1" role="dialog" aria-labelledby="deleteConfirmModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="deleteConfirmModalLabel">Are you sure?</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>
						Do you really want to delete this item?
					</p>
				<div class="modal-footer">

					<button id="btnModalConfirm" type="button" class="btn btn-primary">Yes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

				</div>
			</div>
		</div>
	</div>
@endsection

@section('pageScript')
	<script type="text/javascript">
		$('#deleteConfirmModal').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget); // Button that triggered the modal
			var typeId = button.data('id'); // Extract info from data-* attributes
			// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
			// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
			// var modal = $(this);

			$('#btnModalConfirm').click( function() {

				var modal = $('.modal');
				
				modal.modal('hide');

				$('#deleteForm' + typeId).submit();

			});

		})
	</script>
@endsection