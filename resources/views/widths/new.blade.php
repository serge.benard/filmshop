@extends('layouts.app')

@section('pageTitle', 'New Film Width')

@section('breadcrumbs')

	<ol class="breadcrumb bg-faded">
		<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('widths.index') }}">Film Widths</a></li>
		<li class="breadcrumb-item active">New Film Width</li>
	</ol>

@endsection

@section('content')

	<h1>New Film Width</h1>

	<form action="{{ route('widths.store') }}" method="POST">
		{{ csrf_field() }}
		<fieldset class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
			<div class="input-group">
				<input 	type="number"
						name="amount"
						required="true"
						autocomplete="off"
						step="0.001"
						min="0.001"
						class="input-group-addon"
						id="filmWidthAmount"
					value="{{ old('amount') }}">
				<select name="units" class="input-group-addon">
					@foreach( Units::unitList() as $unit )
						<option value="{{ $unit }}" @if( $preferences->unit == $unit ) selected @endif >
							{{ Units::abbreviations( $unit ) }}
						</option>
					@endforeach
				</select>
			</div>
		</fieldset>
		<fieldset class="form-group">
			<button type="submit" class="btn btn-primary btn-block">
				<span class="fa fa-disk"></span>
				Save
			</button>
			<a href="{{ route('widths.index') }}" class="btn btn-secondary btn-block">
				Cancel
			</a>
		</fieldset>
	</form>

@endsection