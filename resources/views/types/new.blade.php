@extends('layouts.app')

@section('pageTitle', 'New Film Type')

@section('breadcrumbs')

	<ol class="breadcrumb bg-faded">
		<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('types.index') }}">Film Types</a></li>
		<li class="breadcrumb-item active">New Film Type</li>
	</ol>

@endsection

@section('content')

	<h1>New Film Type</h1>

	<form action="{{ route('types.store') }}" method="POST">
		{{ csrf_field() }}
		<fieldset class="form-group{{ $errors->has('category') ? ' has-danger' : '' }}">
			<label for="filmTypeCategory" class="form-control-label">Film Category</label>
			<select name="category"	class="form-control" id="filmTypeCategory">
				<option value="0" {{ old('category') == 0 ? ' selected' : '' }}>
					Please Select Category...
				</option>
				@foreach ($categories as $category)
					<option value="{{ $category->id }}" {{ old('category') == $category->id ? ' selected' : '' }}>
						{{ $category->name }}
					</option>
				@endforeach
			</select>
			<small class="form-control-feedback">
				Required. Please select which Film Category applies to this Film Type.
			</small>
		</fieldset>
		<fieldset class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
			<label for="filmTypeName" class="form-control-label">Film Type Name</label>
			<input 	type="text"
					name="name"
					required="true"
					autocomplete="off" 
					class="form-control"
					id="filmTypeName"
					placeholder="Ex: 121 Etch; Frosted Sandblast"
				value="{{ old('name') }}">
			<small class="form-control-feedback">
				Required. Must be unique.
			</small>
		</fieldset>
		<fieldset class="form-group{{ $errors->has('abbreviation') ? ' has-danger' : '' }}">
			<label for="filmAbbreviationName" class="form-control-label">Film Type Abbreviation</label>
			<input 	type="text"
					name="abbreviation"
					required="true"
					autocomplete="off"
					class="form-control"
					id="filmAbbreviationName"
					placeholder="Ex: 121E; WE; R208MIL"
					value="{{ old('abbreviation') }}"
					minlength="1"
					maxlength="7">
			<small class="form-control-feedback">
				Required. Between 1 and 7 Alpha-Numeric characters only; must be unique.
			</small>
			<small class="text-muted">
				 The abbreviation is incorporated in the identification number used on film rolls entered in inventory. This way, your film rolls are easily identifiable, even just by looking at the roll number.
			</small>
		</fieldset>
		<fieldset class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
			<label for="filmTypeDescription" class="form-control-label">Film Type Description</label>
			<textarea name="description" class="form-control" id="filmTypeDescription" placeholder="Ex: Etch has an etched glass finish.">{{ old('description') }}</textarea>
			<small class="form-control-feedback">
				Optional. Between 2 and 200 characters.
			</small>
		</fieldset>
		<fieldset class="form-group">
			<button type="submit" class="btn btn-primary btn-block">
				<span class="fa fa-disk"></span>
				Save
			</button>
			<a href="{{ route('types.index') }}" class="btn btn-secondary btn-block">
				Cancel
			</a>
		</fieldset>
	</form>

@endsection