@extends('layouts.app')

@section('pageTitle', 'New Film Category')

@section('breadcrumbs')

	<ol class="breadcrumb bg-faded">
		<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('categories.index') }}">Film Categories</a></li>
		<li class="breadcrumb-item active">New Film Category</li>
	</ol>

@endsection

@section('content')

	<h1>New Film Category</h1>

	<form action="{{ route('categories.store') }}" method="POST">
		{{ csrf_field() }}
		<fieldset class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
			<label for="filmCategoryName" class="form-control-label">Film Category Name</label>
			<input 	type="text"
					name="name"
					required="true"
					autocomplete="off" 
					class="form-control"
					id="filmCategoryName"
					placeholder="Ex: Etch, Frost"
				value="{{ old('name') }}">
			<small class="form-control-feedback">
				Required. Must be unique.
			</small>
		</fieldset>
		<fieldset class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
			<label for="filmCategoryDescription" class="form-control-label">Film Category Description</label>
			<textarea name="description" class="form-control" id="filmCategoryDescription" placeholder="Ex: Etch has an etched glass finish.">{{ old('description') }}</textarea>
			<small class="form-control-feedback">
				Optional. Between 2 and 200 characters.
			</small>
		</fieldset>
		<fieldset class="form-group">
			<button type="submit" class="btn btn-primary btn-block">
				<span class="fa fa-disk"></span>
				Save
			</button>
			<a href="{{ route('categories.index') }}" class="btn btn-secondary btn-block">
				Cancel
			</a>
		</fieldset>
	</form>

@endsection