@extends( 'layouts.app' )

@section( 'pageTitle', 'View Film Categories' )

@section('breadcrumbs')

	<ol class="breadcrumb bg-faded">
		<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
		<li class="breadcrumb-item active">Film Categories</li>
	</ol>

@endsection

@section( 'content' )
	<h1>View Film Categories</h1>

	<p>
		<a href="{{ route('categories.create') }}" class="btn btn-info btn-block">
			<span class="fa fa-plus-square-o"></span>
			New Film Category
		</a>
	</p>

	<div class="card-columns">
		
		@foreach ($categories as $category)
		
		<div class="card">

			<div class="card-block">

				<h4 class="card-title h5">
					<a href="{{ route( 'categories.edit', $category->id ) }}">
						{{ $category->name }}
					</a>
				</h4>

				<p class="card-text text-muted text-truncate">
					<small>{{ $category->description or 'No Description Provided.' }}</small>
				</p>

			</div>

			<div class="card-footer">
				<form id="deleteForm{{ $category->id }}" action="{{ route( 'categories.destroy', $category->id ) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}


					<div class="form-group">
						<a href="{{ route('categories.edit', $category->id) }}" class="btn btn-secondary btn-block btn-sm">
							<span class="fa fa-pencil"></span>
							Edit
						</a>
					</div>

					<div class="form-group">
						<button type="button" class="btn btn-outline-danger btn-block btn-sm" data-toggle="modal" data-target="#deleteConfirmModal" data-id="{{ $category->id }}">
							<span class="fa fa-trash"></span> 
							Delete
						</button>
					</div>
					
				</form>
				
			</div>
		</div>
		@endforeach
	</div>

	@if ( $categories->count() < 1 )
	<p>
		There are no Film Categories to display.
	</p>
	@endif

	<p>
		<a href="{{ route('categories.create') }}" class="btn btn-info btn-block">
			<span class="fa fa-plus-square-o"></span>
			New Film Category
		</a>
	</p>
 	
	<div class="modal fade" id="deleteConfirmModal" tabindex="-1" role="dialog" aria-labelledby="deleteConfirmModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="deleteConfirmModalLabel">Are you sure?</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>
						Do you really want to delete this item?
					</p>
				<div class="modal-footer">

					<button id="btnModalConfirm" type="button" class="btn btn-primary">Yes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

				</div>
			</div>
		</div>
	</div>
@endsection

@section('pageScript')
	<script type="text/javascript">
		$('#deleteConfirmModal').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget); // Button that triggered the modal
			var typeId = button.data('id'); // Extract info from data-* attributes
			// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
			// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
			// var modal = $(this);

			$('#btnModalConfirm').click( function() {

				var modal = $('.modal');
				
				modal.modal('hide');

				$('#deleteForm' + typeId).submit();

			});

		})
	</script>
@endsection