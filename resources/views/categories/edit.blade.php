@extends('layouts.app')

@section('pageTitle', 'Edit Film Category')

@section('breadcrumbs')

	<ol class="breadcrumb bg-faded">
		<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('categories.index') }}">Film Category</a></li>
		<li class="breadcrumb-item active">Edit Film Category</li>
	</ol>

@endsection

@section('content')

	<h1>Edit Film Category</h1>

	<form action="{{ route('categories.update', $category->id) }}" method="POST">
		{{ csrf_field() }}
		{{ method_field('PATCH') }}
		<fieldset class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
			<label for="filmTypeName" class="form-control-label">Film Category Name</label>
			<input 	type="text"
					name="name"
					required="true"
					autocomplete="off"
					class="form-control"
					id="filmTypeName"
					placeholder="Ex: Etch, Frost"
				value="{{ old('name', $category->name) }}">
			<small class="form-control-feedback">
				Required. Must be unique, and at least two characters long.
			</small>
		</fieldset>
		<fieldset class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
			<label for="filmTypeDescription" class="form-control-label">Film Category Description</label>
			<textarea 	name="description"
						autocomplete="off"
						class="form-control"
						id="filmTypeDescription"
						placeholder="Ex: Etch has an etched glass finish.">{{ old('description', $category->description) }}</textarea>
			<small class="form-control-feedback">
				Optional. Between 2 and 200 characters.
			</small>
		</fieldset>
		<fieldset class="form-group">
			<button type="submit" class="btn btn-primary btn-block">
				<span class="fa fa-disk"></span>
				Update
			</button>
			<a href="{{ route('categories.index') }}" class="btn btn-secondary btn-block">
				Cancel
			</a>
		</fieldset>
	</form>

@endsection