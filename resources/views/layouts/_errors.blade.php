@if( session('message'))
	<!-- Passed On Message -->
	<div class="alert alert-info" role="alert">
		<span class="fa fa-exclamation-triangle" aria-hidden="true"></span>
		<span class="sr-only">Message:</span>
		{{ session('message') }}
	</div>
@endif
@if( session('error'))
	<!-- Passed On Error -->
	<div class="alert alert-danger" role="alert">
		<span class="fa fa-exclamation-triangle" aria-hidden="true"></span>
		<span class="sr-only">Error:</span>
		{{ session('error') }}
	</div>
@endif
@if( isset( $errors) && count( $errors ) )
	<!-- Form Errors -->
	<div class="alert alert-danger" role="alert">
		<span class="sr-only">Error:</span>
		@foreach( $errors->all() as $error )
			<div>
				<span class="fa fa-exclamation-triangle" aria-hidden="true"></span>
				{{ $error }}
			</div>
		@endforeach
	</div>
@endif

