<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
		data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<a class="navbar-brand" href="{{ url('/') }}">
		{{ config('app.name', 'FilmShop') }}
	</a>
	<div class="collapse navbar-collapse" id="navbarCollapse">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active">
				<a class="nav-link" href="#">Home</a>
			</li>
			<li class="nav-item dropdown">
				<a href="#" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" 
							aria-expanded="false">
					<span class="caret"></span>
					Contacts
				</a>
				<ul class="dropdown-menu">
					<li>
						<a class="dropdown-item" href="{{ route( 'contacts.index' ) }}">
							View
						</a>
					</li>
					<li class="dropdown-divider"></li>
					<li>
						<a class="dropdown-item" href="{{ route( 'contacts.create' ) }}">
							Add
						</a>
					</li>
				</ul>
			</li>
		</ul>
		<ul class="navbar-nav">
			@if (Auth::guest())
				<li class="nav-item"><a class="nav-link" href="{{ url('/login') }}">Login</a></li>
				<li class="nav-item"><a class="nav-link" href="{{ url('/register') }}">Register</a></li>
			@else

				<li class="nav-item dropdown">
					<a href="#" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" 
								aria-expanded="false">
						<span class="fa fa-gear"></span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<li>
							<a class="dropdown-item" href="{{ route('categories.index') }}">
								Film Categories
							</a>
						</li>
						<li>
							<a class="dropdown-item" href="{{ route('types.index') }}">
								Film Types
							</a>
						</li>
						<li>
							<a class="dropdown-item" href="{{ route('widths.index') }}">
								Film Widths
							</a>
						</li>
					</ul>
				</li>

				<li class="nav-item dropdown">
					<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" 
								aria-expanded="false">
						<span class="fa fa-user"></span>
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu" role="menu">
						<li>
							<a class="dropdown-item" href="{{ route('preferences.index') }}">
								Preferences
							</a>
						</li>
						<li class="dropdown-divider"></li>
						</li>

						<li>
							<a class="dropdown-item" href="{{ url('/logout') }}" 
										onclick="event.preventDefault();
										document.getElementById('logout-form').submit();">
								Logout
							</a>

							<form id="logout-form" action="{{ url('/logout') }}" method="POST"
								  style="display: none;">
								{{ csrf_field() }}
							</form>
						</li>
					</ul>
				</li>
			@endif
		</ul>
	</div>
</nav>