<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index' );

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/categories', 'CategoryController');

Route::resource('/types', 'TypeController');

Route::get('/preferences', 'PreferencesController@index')
		->name('preferences.index');
Route::get('/preferences/{preferences}/unit', 'PreferencesController@editUnit')
		->name('preferences.editUnit');
Route::put('/preferences/{preferences}/unit', 'PreferencesController@updateUnit')
		->name('preferences.updateUnit');

Route::resource('/widths', 'WidthsController');

Route::resource('/contacts', 'ContactsController');

// Actions Handled By Resource Controller

// Verb 		URI 					Action 		Route Name
// GET			/photos					index		photos.index
// GET			/photos/create 			create 		photos.create
// POST			/photos					store		photos.store
// GET			/photos/{photo}			show		photos.show
// GET			/photos/{photo}/edit	edit		photos.edit
// PUT/PATCH	/photos/{photo}			update		photos.update
// DELETE		/photos/{photo}			destroy		photos.destroy