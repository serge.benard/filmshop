<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateBaseTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->text('description')->nullable();
        });

        DB::table('categories')->insert([
            [
                'name' => 'Frost',
            ],
            [
                'name' => 'Etch',
            ],
            [
                'name' => 'Security',
            ],
            [
                'name' => 'Solar',
            ],
            [
                'name' => 'Vinyl',
            ],

        ]);

        Schema::create('types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('name')->unique();
            $table->string('abbreviation', 8)->unique();
            $table->text('description')->nullable();

            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });

        DB::table('types')->insert([
            
            
            [
                'category_id' => 1,
                'name' => 'Frosted Sandblast',
                'abbreviation' => 'FS'
            ],
            [
                'category_id' => 1,
                'name' => 'Frosted Crystal',
                'abbreviation' => 'FC'
            ],
            [
                'category_id' => 2,
                'name' => '121 Etch',
                'abbreviation' => 'E'
            ],
            [
                'category_id' => 2,
                'name' => '421 Etch',
                'abbreviation' => 'WE'
            ],
            [
                'category_id' => 3,
                'name' => 'R20 8 Mil',
                'abbreviation' => 'R208Mil'
            ],
            [
                'category_id' => 3,
                'name' => '8 Mil Clear',
                'abbreviation' => '8Mil'
            ],
            [
                'category_id' => 4,
                'name' => 'R20',
                'abbreviation' => 'R20'
            ],
            [
                'category_id' => 4,
                'name' => 'V33 Bronze',
                'abbreviation' => 'V33Br'
            ],
            [
                'category_id' => 5,
                'name' => 'White 3Mil',
                'abbreviation' => 'WV3Mil'
            ],
            [
                'category_id' => 5,
                'name' => 'Goth Black 3Mil',
                'abbreviation' => 'GB3Mil'
            ],

        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('types', 'categories');
    }
}
